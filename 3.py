import cv2
import numpy as np
import pytesseract

img = cv2.imread('input.png')
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV )

#color mask config
h1 = 0
s1 = 127
v1 = 155
h2 = 141
s2 = 255
v2 = 255

#color mask init
h_min = np.array((h1, s1, v1), np.uint8)
h_max = np.array((h2, s2, v2), np.uint8)

#color mask apply
thresh = cv2.inRange(hsv, h_min, h_max)

print(pytesseract.image_to_string(thresh, lang='rus+eng'))